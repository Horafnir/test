(function main() {
  type TAvaibleCoins = { ETH: number, TRON: number, MATIC: number };
  type TRequestedCoins = string[];
  type TCoinDistributionRes = string[];
  function coinDistribution(req1: TAvaibleCoins, req2: TRequestedCoins): TCoinDistributionRes {
    let avaibleCoins = { ...req1 };
    let requestedCoins = [...req2];
    let coinShortage = { ...req1 };
    let res: TCoinDistributionRes = Array(requestedCoins.length).fill('');

    for (let i = 0; i < requestedCoins.length; i++) {
      let coins = requestedCoins[i].split('/');
      for (const coin of coins) {
        coinShortage[coin]--;
      }
    }

    let coinOrder = Object.keys(coinShortage).sort((a, b) => coinShortage[b] - coinShortage[a])
    for (let i = 0; i < requestedCoins.length; i++) {
      let coins = requestedCoins[i].split('/');
      for (const coin of coinOrder) {
        if (coins.indexOf(coin) > -1 && avaibleCoins[coin] > 0) {
          res[i] = coin;
          avaibleCoins[coin]--;
          break
        }
      }
      if (!res[i]) {
        return null
      }
    }

    return res;
  }
}());