import { Controller, Get, Query, Logger, HttpException, HttpStatus } from '@nestjs/common';
import { ConvertService } from './convert.service';
import { TConvertRes, convertReq } from './core'
import { ApiQuery, ApiTags } from '@nestjs/swagger';

@Controller('currency')
export class AppController {
  constructor(private readonly convertService: ConvertService) { }
  private readonly logger = new Logger(AppController.name)

  @Get('convert/')
  @ApiQuery({ name: 'from', required: true })
  @ApiQuery({ name: 'to', required: true})
  @ApiQuery({ name: 'amount', required: true})
  async convertOneToOne(
    @Query() req: convertReq): TConvertRes {
    try{
    const { from, to, amount } = req;
    this.logger.debug('currency /convert', req);
    let res = await this.convertService.convertOneToOne(req);
    return res; 
    } catch (e){
      this.logger.error('currency /convert')
      const err = e as { code: number; message: string };
      if (err.code && err.message) {
        throw new HttpException(err.message, err.code)
      }
      throw new HttpException('InternalServerError', HttpStatus.INTERNAL_SERVER_ERROR)
    }

  }
}
