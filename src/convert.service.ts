import { Injectable, Logger } from '@nestjs/common';
import axios from 'axios';
import { TConvertReq, TConvertRes, TExchangeCalcReq, TExchangeCalcRes, IConvertService } from './core';


@Injectable()
export class ConvertService implements IConvertService{
  private readonly logger = new Logger(ConvertService.name);
  private readonly tstApi = 'https://tstapi.cryptorank.io/v0/coins/prices/';

  async convertOneToOne(req: TConvertReq): TConvertRes {
    this.logger.debug('ConvertService', 'convertOneToOne')
    if(req.amount < 0){
      throw {code: 400, message: 'Wrong amount'}
    }
    const cryptoRankRes = await axios.get(this.tstApi);
    const data = cryptoRankRes.data.data;
    const fromCoin = data.find((coin: { key: string }) => coin.key === req.from);
    const toCoin = data.find((coin: { key: string }) => coin.key === req.to);
    if (!fromCoin || !toCoin) {
      throw { code: 400, message: 'Invalid coin'};
    }
    const result = await this.exchangeСalculation({amount: req.amount, fromPrice: fromCoin.price, toPrice: toCoin.price})
    
    return {
      amount: req.amount, 
      from: req.from, 
      to: req.to, 
      result: result}    
  }
  //since all API pricing data only goes up to 12 decimal places, we get rid of errors by making integers
  async exchangeСalculation(req: TExchangeCalcReq): TExchangeCalcRes{
    const res = + ((Math.floor(req.amount * 1e10) * Math.floor(req.fromPrice * 1e12)) / Math.floor(req.toPrice * 1e12) * 1e-10).toFixed(12);
    if(Number.isFinite(res)){
      return res;
    }else{
      throw {code: 500, message: 'Internal Math Error'}
    }
    
  }
  
}
