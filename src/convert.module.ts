import { Module } from '@nestjs/common';
import { HttpModule } from '@nestjs/axios';
import { AppController } from './convert.controller';
import { ConvertService } from './convert.service';

@Module({
  imports: [ HttpModule ],
  controllers: [ AppController ],
  providers: [ ConvertService ],
})
export class AppModule {}
