import { ApiProperty, ApiPropertyOptional } from '@nestjs/swagger';
import { Type } from 'class-transformer';
import { Min, IsNumber } from 'class-validator';

export class convertReq{
  @ApiProperty()
  from: string;
  
  @ApiPropertyOptional({ default: 'tether'})
  to: string;
  
  @IsNumber()
  @Type(() => Number)
  @ApiPropertyOptional({ default: 1 })
  amount: number;

}