import { type } from "os";

export type TConvertReq = {
  from: string,
  to: string,
  amount: number,
}
export type TConvertRes = Promise<{
  amount: number,
  from: string,
  to: string,
  result: number,
}>

export type TExchangeCalcReq = {
  amount: number, 
  fromPrice: number, 
  toPrice: number
}
export type TExchangeCalcRes = Promise<number>

export interface IConvertService{
  convertOneToOne(req: TConvertReq): TConvertRes;
}