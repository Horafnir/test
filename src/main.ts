import { NestFactory } from '@nestjs/core';
import { Logger, ValidationPipe } from '@nestjs/common'
import { AppModule } from './convert.module';
import { SwaggerModule, DocumentBuilder } from '@nestjs/swagger';

async function bootstrap() {
  const logger = new Logger('EntryPoint');
  const app = await NestFactory.create(AppModule, { logger: ['error', 'warn', 'debug', 'log', 'verbose'] });

  app.useGlobalPipes(new ValidationPipe());

  const config = new DocumentBuilder()
    .setTitle('crypto-rank-test')
    .setDescription('Api Docs for crypto-rank-test')
    .setVersion('1.0')
    .build();

  const document = SwaggerModule.createDocument(app, config);
  SwaggerModule.setup('docs', app, document);

  await app.listen(3000);
  logger.log(`Server running on http://localhost:3000`);
}
bootstrap();  
