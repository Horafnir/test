import { ConvertService } from './convert.service';

const convertStub = {
  from:'ethereum', 
  to:'bitcoin', 
  amount: 100,
}
const exchangeСalculationStub = {amount: 100, fromPrice: 3.111111111111, toPrice: 5.777777777777}

describe('ConvertService', () => {
  let convertService: ConvertService;

  beforeEach(() => {
    convertService = new ConvertService();
  });

  describe('convertOneToOne', () => {
    it('should convert coins correctly', async () => {
      const result = await convertService.convertOneToOne(convertStub);

      expect(result).toEqual({
        amount: 100,
        from: 'ethereum',
        to: 'bitcoin',
        result: expect.any(Number),
      });
    });

    it('should throw an error for invalid coin', async () => {
      await expect(convertService.convertOneToOne({from: '', to:'', amount:100})).rejects.toThrow(
        'Invalid coin',
      );
    });

    it
  });

  describe('exchangeСalculation', () => {
    it('should calculate correctly', async () => {
      const result = await convertService.exchangeСalculation(exchangeСalculationStub)
      expect(Math.floor(result * 1e12)).toEqual( 53846153846159 )
    })

    it('should gave infinity', async () => {
      await expect(convertService.exchangeСalculation({ amount: 100, fromPrice: 3.111111111111, toPrice: 0 })).rejects.toThrow(
        'Internal Server Error',
      );
    })
  })
});